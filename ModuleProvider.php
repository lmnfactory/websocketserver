<?php

namespace Lmn\Websocket\Server;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

use Lmn\Websocket\Server\Lib\Server\WebsocketServer;
use Lmn\Websocket\Server\Lib\Server\Pusher;
use Lmn\Websocket\Server\Lib\Server\BasicConnectionHandler;
use Lmn\Websocket\Server\Lib\Server\AuthConnectionHandler;
use Lmn\Websocket\Server\Command\StartWebsocketServerCommand;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $provider->getApp()->singleton(WebsocketServer::class, function($app) {
            return new WebsocketServer();
        });

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {

    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Websocket\\Server\\Controller'], function() {

        });
    }

    public function registerCommands($provider){
        $app = $provider->getApp();
        $app['lmn.websocketServer.start'] = $provider->getApp()->share(function () {
            return new StartWebsocketServerCommand();
        });

        $provider->commands('lmn.websocketServer.start');
    }
}
