<?php

namespace Lmn\Websocket\Server\Lib\Server;

use Ratchet\MessageComponentInterface;

interface PusherService extends MessageComponentInterface{
    public function setConnectionHandler(ConnectionHandler $handler);
}
