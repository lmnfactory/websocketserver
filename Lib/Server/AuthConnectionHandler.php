<?php

namespace Lmn\Websocket\Server\Lib\Server;

use Ratchet\ConnectionInterface;
use Lmn\Websocket\Server\Lib\Server\ConnectionHandler;
use Lmn\Account\Lib\Auth\AuthService;


class AuthConnectionHandler implements ConnectionHandler {

    private $clients;
    private $connections;
    private $authService;

    public function __construct() {
        $this->connections = new \SplObjectStorage();
        $this->clients = [];
        $this->authService = \App::make(AuthService::class);
    }

    public function onOpen(ConnectionInterface $connection) {
        $this->connections->attach($connection);
        echo "NEW connection.".PHP_EOL;
    }

    public function onMessage(ConnectionInterface $connection, $message) {
        try {
            $user = $this->authService->checkJWTAuth($message);
            if (!isset($this->clients[$user->id])) {
                $this->clients[$user->id] = new \SplObjectStorage();
            }

            if (!$this->clients[$user->id]->contains($connection)) {
                $this->clients[$user->id]->attach($connection);
                echo "REEGISTRATION user: ".$user->id.".".PHP_EOL;
            }
        }
        catch (\Exception $ex) {

        }
    }

    public function onClose(ConnectionInterface $connection) {
        echo "connection ended".PHP_EOL;
        $this->connections->detach($connection);
    }

    public function onError(ConnectionInterface $connection, \Exception $ex) {
        echo "error, closing connection".PHP_EOL;
        $conn->close();
    }

    public function notify($message, $to = null) {
        var_dump($message);
        if ($to == null) {
            return ;
        }

        if (!is_array($to)) {
            $to = [$to];
        }

        foreach ($to as $sendTo) {
            echo "sending to: ".$sendTo.PHP_EOL;
            if (!isset($this->clients[$sendTo])) {
                continue;
            }
            foreach ($this->clients[$sendTo] as $c) {
                $c->send($message);
            }
        }
    }
}
