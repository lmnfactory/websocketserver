<?php

namespace Lmn\Websocket\Server\Lib\Server;

use Ratchet\ConnectionInterface;

class BasicConnectionHandler implements ConnectionHandler {

    private $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage();
    }

    public function onOpen(ConnectionInterface $connection) {
        $this->clients->attach($connection);
    }

    public function onMessage(ConnectionInterface $connection, $message) {
        $connection->send("thx");
    }

    public function onClose(ConnectionInterface $connection) {
        $this->clients->detach($connection);
    }

    public function onError(ConnectionInterface $connection, \Exception $ex) {
        $conn->close();
    }

    public function notify($message, $to = null) {

    }
}
