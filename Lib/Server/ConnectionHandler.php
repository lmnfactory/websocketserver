<?php

namespace Lmn\Websocket\Server\Lib\Server;

use Ratchet\MessageComponentInterface;

interface ConnectionHandler extends MessageComponentInterface {
    public function notify($message, $to = null);
}
