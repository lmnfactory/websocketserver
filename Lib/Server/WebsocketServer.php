<?php

namespace Lmn\Websocket\Server\Lib\Server;

use React\EventLoop\Factory;
use React\Socket\Server;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use React\ZMQ\Context;

class WebsocketServer {

    private $pusherService;
    private $socketAddr;
    private $websocketPort;
    private $websocketAddr;

    public function __construct() {
        $this->pusherService = null;
    }

    public function setPusherService($pusherService) {
        $this->pusherService = $pusherService;
        return $this;
    }

    public function setSocketAddr($socketAddr) {
        $this->socketAddr = $socketAddr;
        return $this;
    }

    public function setWebsocket($addr, $port) {
        $this->websocketPort = $port;
        $this->websocketAddr = $addr;
        return $this;
    }

    public function getWebsocketPort() {
        return $this->websocketPort;
    }

    public function run() {
        if ($this->pusherService == null) {
            throw new \Exception("Push service is not defined.");
        }

        $loop = Factory::create();

        $context = new Context($loop);
        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind($this->socketAddr);
        $pull->on('message', array($this->pusherService, 'notify'));

        $webSock = new Server($loop);
        $webSock->listen($this->websocketPort, $this->websocketAddr);
        $webServer = new IoServer(
            new HttpServer(
                new WsServer($this->pusherService)
            ),
            $webSock
        );

        //$webServer->run();

        $loop->run();
    }
}
