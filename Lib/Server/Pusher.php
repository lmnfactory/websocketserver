<?php

namespace Lmn\Websocket\Server\Lib\Server;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Illuminate\Support\Facades\Log;

class Pusher implements PusherService {

    private $messageHandler;

    public function __construct() {
        $this->messageHandler = null;
    }

    public function setConnectionHandler(ConnectionHandler $handler) {
        $this->messageHandler = $handler;
    }

    public function onOpen(ConnectionInterface $connection) {
        if ($this->messageHandler == null) {
            return ;
        }

        return $this->messageHandler->onOpen($connection);
    }

    public function onMessage(ConnectionInterface $connection, $message) {
        if ($this->messageHandler == null) {
            return ;
        }

        return $this->messageHandler->onMessage($connection, $message);
    }

    public function onClose(ConnectionInterface $connection) {
        if ($this->messageHandler == null) {
            return ;
        }

        return $this->messageHandler->onClose($connection);
    }

    public function onError(ConnectionInterface $connection, \Exception $ex) {
        if ($this->messageHandler == null) {
            return ;
        }

        return $this->messageHandler->onError($connection, $ex);
    }

    public function notify($strMessage) {
        if ($this->messageHandler == null) {
            return ;
        }

        $messageData = json_decode($strMessage, true);
        return $this->messageHandler->notify(json_encode($messageData['message']), $messageData['to']);
    }
}
