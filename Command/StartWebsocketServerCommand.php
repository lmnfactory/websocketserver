<?php

namespace Lmn\Websocket\Server\Command;

use Illuminate\Console\Command;

use Lmn\Websocket\Server\Lib\Server\WebsocketServer;

class StartWebsocketServerCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lmn-websocket:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start websocket server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $websocketServer = \App::make(WebsocketServer::class);
        $this->info("Websocket server running on port ".$websocketServer->getWebsocketPort().".");
        $websocketServer->run();
    }
}
