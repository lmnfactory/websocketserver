<?php

return [
    'name' => 'lmn.websocket-server',

    'provider' => Lmn\Websocket\Server\ModuleProvider::class,
];
